#!/usr/bin/env ruby

require 'rubygems'
require 'nokogiri'
require 'open-uri'

begin
  doc = Nokogiri::HTML(open("https://www.dymo.com/en-US/online-support/dymo-user-guides", "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36"))
rescue
  puts "Failed to fetch download page"
  exit 1
end

link = doc.xpath("//a").find do |link|
  link['href'] && link.text.match(/download/i) && link['href'].match(/.dmg/)
end

puts "#{link['href']}"