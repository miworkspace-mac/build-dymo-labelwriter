#!/bin/bash -ex

# CONFIG
prefix="DYMOLabelwriter"
suffix=""
munki_package_name="DYMOLabelwriter"
display_name="DYMO Labelwriter"
category="Productivity"
#url=`./finder.rb`
url="https://download.dymo.com/dymo/Software/Mac/DLS8Setup.8.7.5.dmg"

# download it (-L: follow redirects)
#curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36' "${url}"
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.3 Safari/605.1.15' "${url}"

## Mount disk image on temp space
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
cp "${mountpoint}"/* DYMOLabel.pkg
/usr/sbin/pkgutil --expand DYMOLabel.pkg pkg
mkdir build-root
(cd build-root; pax -rz -f ../pkg/component.pkg/Payload)
hdiutil detach "${mountpoint}"

mkdir build-app
mkdir build-app/Applications
mv "build-root/DYMO Label.app" build-app/Applications

# edit postinstall of component10
sed -i .bak -e '/# Launch web service/,$d' pkg/component-10.pkg/scripts/postinstall

## Putting it back together
/usr/sbin/pkgutil --flatten pkg dymo.pkg

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-app/Applications -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root dymo.pkg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-app//' app.plist

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "print :CFBundleShortVersionString" "build-app/Applications/DYMO Label.app/Contents/Info.plist"`

plist=`pwd`/app.plist

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.12.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" version "${version}"
defaults write "${plist}" category -string "${category}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv dymo.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
